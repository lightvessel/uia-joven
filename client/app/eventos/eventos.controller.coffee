'use strict'

angular.module 'uiaJovenFullApp'
.controller 'EventosCtrl', ($http,$scope, dialogs, Evento)  ->
  $scope.setEntitiesUrl   '/api/eventos'
  $scope.setEntityName    'Eventos'
  $scope.setPopupTemplate 'edit_evento'
  $scope.setEntityService  Evento
  $scope.initParent()
