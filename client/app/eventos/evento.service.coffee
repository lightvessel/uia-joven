'use strict'

angular.module 'uiaJovenFullApp'
.factory 'Evento', ($resource) ->
  $resource '/api/eventos/:id',
    id: '@_id'
  ,
    update:
      method: 'PUT'

    save:
      method: 'POST'

    get:
      method: 'GET'
