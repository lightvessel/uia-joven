'use strict'

angular.module 'uiaJovenFullApp'
.controller 'EventoCtrl', ($scope,$stateParams, Evento) ->
  $scope.setEventoId = (id) -> $scope.eventoId = id
  $scope.initParent = ()->
    Evento.get id: $scope.eventoId, (e) ->
      $scope.evento = e
