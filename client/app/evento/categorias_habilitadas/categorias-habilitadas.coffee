'use strict'

angular.module 'uiaJovenFullApp'
.config ($stateProvider) ->
  $stateProvider.state 'main.evento.categoriasHabilitadas',
    url: '/evento/:id/categorias-habilitadas'
    parent: 'main.evento'
    templateUrl: 'app/main/evento/categorias_habilitadas/categorias-habilitadas.html'
    controller: 'CategoriasHabilitadasCtrl'
