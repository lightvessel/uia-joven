'use strict'

angular.module 'uiaJovenFullApp'
.controller 'CategoriasHabilitadasCtrl', ($http, $scope,$stateParams, CategoriasHabilitadas) ->
  $scope.setEventoId $stateParams.id
  $scope.categoriaSeleccionada = {nombre:''}
  $scope.initParent()
  $http.get '/api/categorias'
  .success (entities) ->
    $scope.categoriasGlobales = entities

  $scope.agregarCategoria = () ->
    console.log $scope.categoriaSeleccionada
    if $scope.categoriaSeleccionada._id
    then CategoriasHabilitadas.addCategory {id:$stateParams.id, catId:$scope.categoriaSeleccionada._id}


