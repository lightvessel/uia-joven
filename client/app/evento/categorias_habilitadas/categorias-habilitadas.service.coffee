'use strict'

angular.module 'uiaJovenFullApp'
.factory 'CategoriasHabilitadas', ($resource) ->
  $resource '/api/eventos/:id/categorias/:catId',
    id: '@id'
    catId:'@catId'
  ,
    removeCategory:
      method: 'DELETE'

    addCategory:
      method: 'PUT'
