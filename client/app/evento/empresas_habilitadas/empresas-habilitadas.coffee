'use strict'

angular.module 'uiaJovenFullApp'
.config ($stateProvider) ->
  $stateProvider.state 'main.evento.empresasHabilitadas',
    url: '/evento/:id/empresas-habilitadas'
    parent: 'main.evento'
    templateUrl: 'app/main/evento/empresas_habilitadas/empresas-habilitadas.html'
    controller: 'EmpresasHabilitadasCtrl'
