'use strict'

angular.module 'uiaJovenFullApp'
.factory 'Empresa', ($resource) ->
  $resource '/api/empresas/:id',
    id: '@_id'
  ,
    update:
      method: 'PUT'

    save:
      method: 'POST'

    get:
      method: 'GET'
