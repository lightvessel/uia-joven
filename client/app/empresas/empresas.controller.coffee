'use strict'

angular.module 'uiaJovenFullApp'
.controller 'EmpresasCtrl', ($http,$scope, dialogs, Empresa) ->
  $scope.setEntitiesUrl   '/api/empresas'
  $scope.setEntityName    'Empresa'
  $scope.setPopupTemplate 'edit_empresa'
  $scope.setEntityService  Empresa
  $scope.initParent()
