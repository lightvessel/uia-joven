'use strict'

angular.module 'uiaJovenFullApp'
.filter 'siNoBool', ()-> (e)-> return e? 'Sí':'No'
.controller 'ListableCtrl', ($http,$scope, $resource, $state, $filter, dialogs, ngTableParams, $sce) ->

  $scope.setEntitiesUrl       = (e)-> $scope.entitiesUrl = e
  $scope.setOnEntitiesLoaded  = (e)-> $scope.onEntitiesLoaded = e
  $scope.setEntityName        = (e)-> $scope.entityName = e
  $scope.setPopupTemplate     = (e)-> $scope.popupTemplate = e+'.html'


  $scope.siNoBool = (condicion)-> condicion? 'Sí':'No'
  $scope.setEntityService = (e)-> $scope.entityService = e

  $scope.setSomeValue = (k,v) -> $scope[k] = v
  $scope.today = new Date()
  $scope.seleccionada = {
    entity:{}
  }

  $scope.entities = []
  $scope.table = { isValid: true, errors:""}

  $scope.initParent = ()->
    $http.get $scope.entitiesUrl
    .success (entities) ->
      $scope.entities = entities
      if $scope.tableParams then $scope.tableParams.sorting({})
      if $scope.onEntitiesLoaded then $scope.onEntitiesLoaded(entities)

      $scope.tableParams = new ngTableParams(
        page: 1 # show first page
        count: 25 # count per page
        sorting:
          name: "asc" # initial sorting
      ,
        total: $scope.entities.length # length of data
        getData: ($defer, params) ->
          data = $scope.entities
          orderedData = (if params.sorting() then $filter("orderBy")(data, params.orderBy()) else data)
          $defer.resolve orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count())
      )


    $scope.delete = (entity) ->
      $scope.entityService.remove id: entity._id
      _.remove $scope.entities, entity

    $scope.registrar = (entity, handler) ->
      $scope.seleccionada.entity = entity
      dialogs.create(
        $scope.popupTemplate,
        'DialogEditionCtrl',
        {data:$scope.seleccionada.entity, handler:handler},
        {size:'lg'}
      )

    $scope.saveEntity  = (entity)-> $scope.registrar entity, (data, onSuccess, onError) ->
      $scope.entityService.save data,
        (saved) ->
          $scope.entities.push(saved)
          onSuccess()
      ,(err) -> onError(err)

    $scope.guardar = (data, onSuccess, onError)  ->
      $scope.entityService.update data,
        (updated) ->
          $scope.seleccionada.entity = updated
          onSuccess()
      ,(err) -> onError(err)

    $scope.updateEntity = (entity)-> $scope.registrar entity, $scope.guardar

    $scope.goTo = (id) ->
      $state.go('formulario()')
