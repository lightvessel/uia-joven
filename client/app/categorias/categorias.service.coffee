'use strict'

angular.module 'uiaJovenFullApp'
.factory 'Categoria', ($resource) ->
  $resource '/api/categorias/:id',
    id: '@_id'
  ,
    update:
      method: 'PUT'

    save:
      method: 'POST'

    get:
      method: 'GET'
