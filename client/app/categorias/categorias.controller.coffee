'use strict'

angular.module 'uiaJovenFullApp'
.controller 'CategoriasCtrl', ($http,$scope, dialogs, Categoria) ->

  $scope.setEntitiesUrl   '/api/categorias'
  $scope.setEntityName    'Categoría'
  $scope.setPopupTemplate 'edit_categoria'
  $scope.setEntityService  Categoria
  $scope.initParent()
