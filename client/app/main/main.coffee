'use strict'

angular.module 'uiaJovenFullApp'
.config ($stateProvider) ->
  $stateProvider
  .state 'main',
    templateUrl: 'app/main/main.html'
    controller: 'MainCtrl'
    abstract:true
    authenticate: true
  .state 'main.listable',
    abstract:true
    parent: 'main'
    templateUrl: 'app/listable/listable.html'
    controller: 'ListableCtrl'
  .state 'main.listable.eventos',
    url: '/eventos'
    parent: 'main.listable'
    templateUrl: 'app/eventos/eventos.html'
    controller: 'EventosCtrl'
    authenticate: true
  .state 'main.listable.categorias',
    url: '/categorias'
    parent: 'main.listable'
    templateUrl: 'app/categorias/categorias.html'
    controller: 'CategoriasCtrl'
    authenticate: true

  .state 'main.listable.empresas',
    url: '/empresas'
    parent: 'main.listable'
    templateUrl: 'app/empresas/empresas_listado.html'
    controller: 'EmpresasCtrl'
    authenticate: true
  .state 'main.evento',
    abstract: true
    parent: 'main'
    templateUrl: 'app/evento/evento.html'
    controller: 'EventoCtrl'
    authenticate: true
  .state 'main.listable.home',
    url: '/'
    parent: 'main.listable'
    templateUrl: 'app/eventos/eventos.html'
    controller: 'EventosCtrl'
    authenticate: true
