angular.module 'uiaJovenFullApp'
.controller 'DialogEditionCtrl', ($http, $scope,$modalInstance,data) ->
  $scope.opened = false

  $scope.errors = {}
  $scope.seleccionada = {
    categorias: []
  }
  $scope.data = data.data
  $scope._ = _ # Agregamos lodash al scope
  $scope.acumAsientos = (sum,grupo) -> grupo.lugares + sum

  $scope.removeCategoria = (index)->
    $scope.seleccionada.categorias.splice(index,1)
    $scope.data.grupos.splice(index,1)
    console.log($scope.data.grupos)

  $scope.addCategoria = ()->
    $scope.data.grupos.push($scope.seleccionada.categoria._id)
    $scope.seleccionada.categorias.push($scope.seleccionada.categoria)
    $scope.seleccionada.categoria = undefined

  $http.get '/api/categorias'
  .success (cats) ->
    $scope.categorias = cats
    _.forEach $scope.data.grupos, (id)->
      $scope.seleccionada.categorias.push(_.find cats, _id:id)

  $scope.open = ($event)->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.opened = true

  $scope.done = (form)->
    data.handler $scope.data,
      () ->
        data.data.grupos = $scope.data.grupos
        $modalInstance.close($scope.data)
      ,(err) ->
        err = err.data
        $scope.errors = {}
        # Update validity of form fields that match the mongoose errors
        angular.forEach err.errors, (error, field) ->
          form[field].$setValidity 'mongoose', false
          $scope.errors[field] = error.message

.config (dialogsProvider) ->
  dialogsProvider.useBackdrop(true)
  dialogsProvider.useEscClose(true)
  dialogsProvider.useCopy(true)
  dialogsProvider.setSize('sm')
