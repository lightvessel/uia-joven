'use strict'

angular.module 'uiaJovenFullApp'
.config ($stateProvider) ->
  $stateProvider.state 'formulario',
    url: '/formulario/:id'
    templateUrl: 'app/formulario/formulario.html'
    controller: 'FormularioCtrl'
    authenticate: false
