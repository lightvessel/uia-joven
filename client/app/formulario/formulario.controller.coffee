'use strict'

angular.module 'uiaJovenFullApp'
.controller 'FormularioCtrl', ($http,$scope, $stateParams, Evento, Inscripto) ->

  Evento.get id: $stateParams.id, (e) ->
    $scope.evento = e

  $scope.inscripcion = { exitosa:false }

  $http.get '/api/empresas'
  .success (empresas)->
    $scope.empresas = empresas

  $http.get '/api/categorias'
  .success (categorias)->
    $scope.categorias = categorias

  $scope.inscripto =
    nombre: ''
    apellido: ''
    dni: undefined
    cargo: ''
    telefono: ''
    email: ''
    empresa: {}
    categoria: {}

  $scope.mensajeDeError = ''
  $scope.mensajeDeExito = ''

  $scope.done = (form) ->
    $scope.mensajeDeExito  = $scope.inscripto.categoria.mensajeExitoso
    $scope.mensajeDeError  = $scope.inscripto.categoria.mensajeEmpresaInvalida
    if $scope.inscripto.empresa then $scope.inscripto.empresa = $scope.inscripto.empresa._id
    $scope.inscripto.categoria = $scope.inscripto.categoria._id
    Inscripto.save $scope.inscripto,
      (saved)->
        console.log(saved)
        $scope.inscripcion.exitosa = true
    ,(err) ->
      err = err.data
      $scope.errors = {}
      # Update validity of form fields that match the mongoose errors
      angular.forEach err.errors, (error, field) ->
        form[field].$setValidity 'mongoose', false
        $scope.errors[field] = error.message
