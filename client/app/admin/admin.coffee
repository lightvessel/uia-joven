'use strict'

angular.module 'uiaJovenFullApp'
.config ($stateProvider) ->
  $stateProvider
  .state 'admin',
    url: '/admin'
    authenticate: true
    templateUrl: 'app/admin/admin.html'
    controller: 'AdminCtrl'
