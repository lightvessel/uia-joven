'use strict'

angular.module 'uiaJovenFullApp'
.config ($stateProvider) ->
  $stateProvider
  .state 'main.listable.inscriptos',
    url: '/inscriptos'
    parent: 'main.listable'
    templateUrl: 'app/inscriptos/inscriptos.html'
    controller: 'InscriptosCtrl as InscriptoControlador'
    authenticate: true
