'use strict'

angular.module 'uiaJovenFullApp'
.controller 'InscriptosCtrl', ($scope, $filter, $http, socket, dialogs, Inscripto)  ->

  $scope.setEntitiesUrl   '/api/inscriptos'
  $scope.setEntityName    'Inscriptos'
  $scope.setPopupTemplate 'edit_inscripto'
  $scope.setEntityService  Inscripto

  $scope.setSomeValue 'guardarRow', (inscripto) ->
    inscripto.categoria = inscripto.categoria._id
    $scope.guardar inscripto, (saved)->
      inscripto.$edit = false
      inscripto.categoria = _.find $scope.categorias, _id:inscripto.categoria
    ,(err) ->
      $scope.table.isValid = false
      $scope.table.errors = err

  $scope.setOnEntitiesLoaded (inscriptos)->
    $scope.tableParams.reload()
    $http.get '/api/categorias'
    .success (cats) ->
      $scope.categorias = cats
      _.forEach inscriptos, (inscripto)->
        inscripto.categoria = _.find cats, _id:inscripto.categoria

  $scope.initParent()
