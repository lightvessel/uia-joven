'use strict'

angular.module 'uiaJovenFullApp'
.factory 'Inscripto', ($resource) ->
  $resource '/api/inscriptos/:id',
    id: '@_id'
  ,
    update:
      method: 'PUT'

    save:
      method: 'POST'

    get:
      method: 'GET'
