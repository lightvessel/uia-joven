/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/categorias', require('./api/categoria'));
  app.use('/api/eventos', require('./api/evento'));
  app.use('/api/habilitados', require('./api/habilitado'));
  app.use('/api/empresas', require('./api/empresa'));
  app.use('/api/inscriptos', require('./api/inscripto'));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets|templates)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
