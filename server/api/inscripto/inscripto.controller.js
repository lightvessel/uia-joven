'use strict';

var _ = require('lodash');
var Inscripto = require('./inscripto.model');
var Categoria = require('../categoria/categoria.model');
var Empresa = require('../empresa/empresa.model');
var ObjectId = require('mongoose').Types.ObjectId;

// Get list of inscriptos
exports.index = function(req, res) {
  Inscripto.find(function (err, inscriptos) {
    if(err) { return handleError(res, err); }
    return res.json(200, inscriptos);
  });
};

// Get a single inscripto
exports.show = function(req, res) {
  Inscripto.findById(req.params.id, function (err, inscripto) {
    if(err) { return handleError(res, err); }
    if(!inscripto) { return res.send(404); }
    return res.json(inscripto);
  });
};

// Creates a new inscripto in the DB.
exports.create = function(req, res) {
  var inscripto = req.body;
  Categoria.findById(inscripto.categoria, function(err, cat){
    if(err) return handleError(res, err);
    else {
      if(cat.validarEmpresa){
        Empresa.findById( inscripto.empresa).populate('grupos', 'lugares').exec(function(err, em) {
          if (err) return handleError(res, err);
          else {
            if(!em) {
              return res.json(500, {"message":"Falló la validación","name":"ValidationError","errors":{"categoria":{
                "message":"La categoría anteriormente seleccionada requiere una empresa","name":"ValidatorError","path":"categoria","type":"required","value":null}}});
            } else if(em.cuit != inscripto.cuit){
              return res.json(500, {"message":"Falló la validación","name":"ValidationError","errors":{"cuit":{
                "message":"El campo CUIT no corresponde con la empresa seleccionada","name":"ValidatorError","path":"cuit","type":"required","value":null}}});
            } else {
              Inscripto.count({empresa:new ObjectId(em._id)},function(err, count) {
                var tope = _.reduce(em.grupos, function(acum, grupo){return grupo.lugares + acum;},0);
                if(count >= tope){
                  return res.json(500, {"message":"Falló la validación","name":"ValidationError","errors":{"empresa":{
                    "message":cat.mensajeEmpresaInvalida,"name":"ValidatorError","path":"empresa","type":"required","value":null}}});
                } else {
                  Inscripto.create(req.body, function(err, saved) {
                    return res.json(201, saved);
                  });
                }
              } );
            }
          }
        });
      } else {
        Inscripto.create(req.body, function(err, saved) {
          return res.json(201, saved);
        });
      }
    }
  });
};

// Updates an existing inscripto in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Inscripto.findById(req.params.id, function (err, inscripto) {
    if (err) { return handleError(res, err); }
    if(!inscripto) { return res.send(404); }
    var updated = _.merge(inscripto, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, inscripto);
    });
  });
};

// Deletes a inscripto from the DB.
exports.destroy = function(req, res) {
  Inscripto.findById(req.params.id, function (err, inscripto) {
    if(err) { return handleError(res, err); }
    if(!inscripto) { return res.send(404); }
    inscripto.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
