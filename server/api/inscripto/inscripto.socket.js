/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Inscripto = require('./inscripto.model');

exports.register = function(socket) {
  Inscripto.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Inscripto.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('inscripto:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('inscripto:remove', doc);
}