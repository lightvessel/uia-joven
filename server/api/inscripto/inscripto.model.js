'use strict';

var mongoose = require('mongoose'),
    validate = require('mongoose-validator'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.Types.ObjectId;

var emailValidator = [
  validate({
    validator:'isEmail',
    message:'El mail ingresado es inválido'
  })
];

var InscriptoSchema = new Schema({
  nombre: {
    type: String,
    default: '',
    required: 'El campo Nombre es obligatorio',
    trim: true
  },
  apellido: {
    type: String,
    default: '',
    required: 'El campo Apellido es obligatorio',
    trim: true
  },
  dni: {
    type: Number,
    default: '',
    required: 'El campo DNI es obligatorio',
    trim: true
  },
  cargo: {
    type: String,
    default: '',
    required: 'El campo Cargo es obligatorio',
    trim: true
  },
  cuit: {
    type: Number,
    default: '',
    required: 'El campo CUIT es obligatorio, no incluir guiones ni espacios',
    trim: true
  },
  telefono: {
    type: String,
    default: '',
    required: 'El campo Teléfono es obligatorio',
    trim: true
  },
  email: {
    type: String,
    default: '',
    validator: emailValidator,
    required: 'El campo eMail es obligatorio',
    trim: true
  },
  pago: {type:Boolean, default:false},
  asistio: {type:Boolean, default:false},
  empresa: { type : ObjectId, ref: 'Empresa' },
  categoria: { type : ObjectId, ref: 'Categoria', required: 'El campo Categoría es obligatorio', },
  created: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Inscripto', InscriptoSchema);
