'use strict';

var express = require('express');
var controller = require('./evento.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

router.put('/:id/categorias/:catId',    controller.addCategory);
router.delete('/:id/categorias/:catId', controller.removeCategory);

module.exports = router;
