'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.Types.ObjectId;

var EventoSchema = new Schema({
  nombre: {
    type: String,
    required: 'El campo Nombre es obligatorio',
    trim: true
  },
  descripcion:String,
  empresasHabilitadas:[{ type : ObjectId, ref: 'Empresa' }],
  categoriasHabilitadas:[{ type : ObjectId, ref: 'Categoria' }],
  inscriptos:[{ type : ObjectId, ref: 'Inscripto' }],
  capacidad: Number,
  fecha: Date
});

module.exports = mongoose.model('Evento', EventoSchema);
