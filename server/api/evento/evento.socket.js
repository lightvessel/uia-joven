/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Evento = require('./evento.model');

exports.register = function(socket) {
  Evento.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Evento.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('evento:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('evento:remove', doc);
}