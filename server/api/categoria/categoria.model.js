'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CategoriaSchema = new Schema({
  nombre: String,
  detalle:String,
  lugares: Number,
  esPaga:Boolean,
  costo:Number,
  validarEmpresa: Boolean,
  mensajeExitoso:String,
  mensajeFaltaValidarEmpresa:String,
  mensajeEmpresaInvalida:String
});

module.exports = mongoose.model('Categoria', CategoriaSchema);
