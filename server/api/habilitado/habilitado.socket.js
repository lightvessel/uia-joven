/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Habilitado = require('./habilitado.model');

exports.register = function(socket) {
  Habilitado.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Habilitado.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('habilitado:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('habilitado:remove', doc);
}