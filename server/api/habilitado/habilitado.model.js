'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId;

var HabilitadoSchema = new Schema({
  empresa:{ type : mongoose.Schema.ObjectId, ref: 'Empresa' },
  categorias:[{ type : ObjectId, ref: 'Categoria' }]
});

module.exports = mongoose.model('Habilitado', HabilitadoSchema);
