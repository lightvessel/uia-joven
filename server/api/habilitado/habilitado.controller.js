'use strict';

var _ = require('lodash');
var Habilitado = require('./habilitado.model');

// Get list of habilitados
exports.index = function(req, res) {
  Habilitado.find(function (err, habilitados) {
    if(err) { return handleError(res, err); }
    return res.json(200, habilitados);
  });
};

// Get a single habilitado
exports.show = function(req, res) {
  Habilitado.findById(req.params.id, function (err, habilitado) {
    if(err) { return handleError(res, err); }
    if(!habilitado) { return res.send(404); }
    return res.json(habilitado);
  });
};

// Creates a new habilitado in the DB.
exports.create = function(req, res) {
  Habilitado.create(req.body, function(err, habilitado) {
    if(err) { return handleError(res, err); }
    return res.json(201, habilitado);
  });
};

// Updates an existing habilitado in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Habilitado.findById(req.params.id, function (err, habilitado) {
    if (err) { return handleError(res, err); }
    if(!habilitado) { return res.send(404); }
    var updated = _.merge(habilitado, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, habilitado);
    });
  });
};

// Deletes a habilitado from the DB.
exports.destroy = function(req, res) {
  Habilitado.findById(req.params.id, function (err, habilitado) {
    if(err) { return handleError(res, err); }
    if(!habilitado) { return res.send(404); }
    habilitado.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}