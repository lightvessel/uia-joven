'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.Types.ObjectId;

var validate = require('mongoose-validator');

var urlValidator = [validate({validator:'isURL', message:'El formato de la URL es incorrecto',passIfEmpty: true})];

var EmpresaSchema = new Schema({
  nombre: {type: String,required: 'El campo Nombre es obligatorio',trim: true},
  cuit: {type: Number, required: 'El campo CUIT es obligatorio'},
  web: {type:String, validate:urlValidator},
  logo: {type:String,validate:urlValidator},
  grupos: [{ type : ObjectId, ref: 'Categoria' }],
  comentarios:String
});

module.exports = mongoose.model('Empresa', EmpresaSchema);
